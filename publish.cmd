setlocal
set VERSION=1
rd /s /q "%~dp0publish"
rd /s /q "%~dp0HelloWorld_NetCore_MVC\bin"
rd /s /q "%~dp0HelloWorld_NetCore_MVC\obj"
dotnet publish -c Release -o "%~dp0publish\HelloWorld_NetCore_MVC" "%~dp0HelloWorld_NetCore_MVC\HelloWorld_NetCore_MVC.csproj"
"%ProgramFiles%\7-Zip\7z.exe" a "%~dp0publish\HelloWorld_NetCore_MVC_v%VERSION%.zip" "%~dp0publish\HelloWorld_NetCore_MVC\*"
